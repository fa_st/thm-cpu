#THM Rechnerarchitektur WS 2013/14 CPU

##Instructions:

```
Name	OpCode	Data	Function
---------------------------------------------------------
LOAD	000	aaaaa	Acc <- Mem
STORE	001	aaaaa	Mem <- Acc

ADD	010	aaaaa	Acc <- Acc + Mem
SUB	011	aaaaa	Acc <- Acc - Mem
NAND	100	bbbbb	Acc <- not(Acc and Mem)
IN	100	00000	Acc <- Input (Waits for in_enter)
OUT	100	00001	Out <- Acc

JZ	101	aaaaa	PC Jump if Acc = 0
JPOS	110	aaaaa	PC Jump if Acc > 0
J	111	aaaaa	PC Jump always
---------------------------------------------------------

aaaaa = Memory Address
bbbbb = Memory Address > 0x01
```

###Clock Cycles per Instruction

|Instructions|Clocks|
|---|---|
|LOAD, STORE, ADD, SUB, NAND | 5 |
OUT, JZ, JPOS, J | 4 |
RESET | 3 |
IN |4 + in_enter |

RESET perform a 3 cycle init process to load PC, MEM, IR and CTRL
IN waits until the in_enter input is active

---

##Control Unit State Machine:

```
RESET           -- Reset CPU

CTRL_LOAD_IR    -- New Instruction from IR

MEM_STORE       -- Write Memory In

ACC_MEM         -- Acc load Memory

ACC_ALU_ADD     -- Acc load ALU with ALU-Add Operation
ACC_ALU_SUB     -- Acc load ALU with ALU-Sub Operation
ACC_ALU_NAND    -- Acc load ALU with ALU-Nand Operation

ACC_IN_ENTER    -- Acc load Key-Input on in_enter

JUMP_PC_MEM     -- PC and MEM Jump to Address

NOP_PC          -- Update PC
NOP_OUT         -- Update PC + Enable Output
NOP_MEM         -- Update MEM
NOP_IR          -- Update IR
```

##Simulation Program

First simulation-program:

```
-- Some Values
ram(31) <= "00000110";      -- Value: 0x06
ram(30) <= "00000101";      -- Value: 0x05
ram(29) <= "00000100";      -- Value: 0x04
ram(28) <= "00000011";      -- Value: 0x03
ram(27) <= "00000010";      -- Value: 0x02
ram(26) <= "00000001";      -- Value: 0x01
ram(25) <= "00000000";      -- Value: 0x00

-- Default Program
--
--          Ins  |  Addr
ram(00) <= "000" & "11111"; -- LOAD     ram(31) A = 0x06
ram(01) <= "010" & "11010"; -- ADD      ram(26) A = 0x07
ram(02) <= "001" & "11000"; -- STORE    ram(24) A = 0x07
ram(03) <= "100" & "00001"; -- OUT              A = 0x07
ram(04) <= "111" & "00000"; -- JUMP to ram(0)
```

![Logicsimualtion](sim.png "Logicsimulation")

