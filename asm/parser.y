%{

#include <stdio.h>

int    yylex(void);
void   yyerror(const char *);
FILE * yyin;

%}

%token INSTR_LOD INSTR_STO INSTR_ADD INSTR_SUB INSTR_NAN INSTR_INP INSTR_OUT INSTR_JMP INSTR_JMZ INSTR_JMA 
%token MEM_ADR
%token IDENTIFIER NUMBER COMMA COLON SEMICOLON

%start program

%%

mem_adr
	: MEM_ADR
	;

label
   : IDENTIFIER COLON
   ;

instruction_sto
	: INSTR_STO mem_adr SEMICOLON
	;
	
instruction_add
	: INSTR_ADD mem_adr SEMICOLON
	;

instruction_sub
	: INSTR_SUB mem_adr SEMICOLON
	;

instruction_nan
	: INSTR_NAN mem_adr SEMICOLON
	;

instruction_inp
	: INSTR_INP SEMICOLON
	;

instruction_out
	: INSTR_OUT SEMICOLON
	;

instruction_jmp
	: INSTR_JMP mem_adr SEMICOLON
	;

instruction_jmz
	: INSTR_JMZ mem_adr SEMICOLON
	;
	
instruction_jma
	: INSTR_JMA mem_adr SEMICOLON
	;
	
instruction
   : instruction_sto
   | instruction_add
   | instruction_sub
   | instruction_inp
   | instruction_out
   | instruction_jmp
   | instruction_jmz
   | instruction_jma
   ;

program
   : instruction
   | label
   | program instruction
   | program label
   ;

%%

void yyerror(const char * message)
{
   printf(message);
   puts("\n");
}

int main(int argc, char **argv)
{
   if(argc == 2)
   {
      if((yyin = fopen(argv[1], "rb")) != NULL)
      {
         yyparse();
         puts("Hello\n");
         fclose(yyin);
      }
   }

   return 0;
}


