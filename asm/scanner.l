%{

#include "parser.tab.h"

%}

%option noyywrap
%option pointer

CT       "//"[^\n]*
WS       [ \n\r\t]+
ID       [_a-zA-Z][_a-zA-Z0-9]*
NUM      [0-9]+
ADR		 [0x][0-9A-F]

%%

{CT}     {}
{WS}     {}

"lod"    { return INSTR_LOD; }
"sto"	 { return INSTR_STO; }
"add"    { return INSTR_ADD; }
"sub"    { return INSTR_SUB; }
"nan"	 { return INSTR_NAN; }

"inp"	 { return INSTR_INP; }
"out"	 { return INSTR_OUT; }

"jmp"    { return INSTR_JMP; }
"jmz"	 { return INSTR_JMZ; }
"jma"	 { return INSTR_JMA; }


{ADR}	 { return MEM_ADR;   }

{ID}     { return IDENTIFIER; }
{NUM}    { return NUMBER; }

","      { return COMMA; }
":"      { return COLON; }
";"      { return SEMICOLON; }

.        { yyerror("syntax error"); }

