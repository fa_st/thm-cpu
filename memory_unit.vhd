-- ------------------------ --
--  THM CPU RA WS 2013/14   --
--                          --
--  Memory                  --
-- ------------------------ --
-- Author:  Matthias Roell  --
-- Date:    06.02.2014      --
-- ------------------------ --

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity memory_unit is
port
(
    -- Control Input --
    clk         : in    std_logic;
    reset       : in    std_logic;
    mem_write   : in    std_logic;
    -- Data Input --
    address     : in    std_logic_vector(4 downto 0); 
    acc_out     : in    std_logic_vector(7 downto 0);
    -- Data Output  --
    mem_out     : out   std_logic_vector(7 downto 0)
);
end memory_unit;

architecture rtl of memory_unit is

type ram_type is array (31 downto 0) of std_logic_vector(7 downto 0);
signal ram          : ram_type;
signal read_address : std_logic_vector(4 downto 0);

begin

RAM_PROCESS:
    process(clk, reset)
    begin
        if reset = '1' then
            -- Default Code
            
            -- Some Values
            ram(31) <= "00000110";      -- Value: 0x06
            ram(30) <= "00000101";      -- Value: 0x05
            ram(29) <= "00000100";      -- Value: 0x04
            ram(28) <= "00000011";      -- Value: 0x03
            ram(27) <= "00000010";      -- Value: 0x02
            ram(26) <= "00000001";      -- Value: 0x01
            ram(25) <= "00000000";      -- Value: 0x00
            
            -- Default Program
            --
            --          Ins  |  Addr
            ram(00) <= "000" & "11111"; -- LOAD     ram(31) A = 0x06
            ram(01) <= "010" & "11010"; -- ADD      ram(26) A = 0x07
            ram(02) <= "001" & "11000"; -- STORE    ram(24) A = 0x07
            ram(03) <= "100" & "00001"; -- OUT              A = 0x07
            ram(04) <= "111" & "00000"; -- JUMP to ram(0)
            
        elsif rising_edge(clk) then
            -- Write data to memory
            if mem_write = '1' then
                ram(to_integer(unsigned(address))) <= acc_out;
            end if;
            -- Store output adress
            read_address <= address;
        end if;
    end process;

-- Set output (updates with RAM_PROCESS)
mem_out <= ram(to_integer(unsigned(read_address)));

end;
